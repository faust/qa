#use wml::templ::template title="Bugs tagged security" author="Martin Michlmayr"

This page <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=security">moved</A>.

__END__

<P>This page lists all bugs which have been tagged <I>security</I>.
Please go through this listing, click on those bugs where you think you
might be able to help and send in comments or patches through the BTS
that help solving the bug.  If you see that an important security problem
has not been fixed for a long time, contact the maintainer of the package
and NMU if you don't get a response within a reasonable time.  Also, work
with the <A HREF = "http://security.debian.org">Security Team</A> to get
out a new version if the security problem applies to the package in stable.

<:

use POSIX qw(strftime);
use HTML::Entities;

sub protect {
    "<protect pass=4->" . encode_entities($_[0], "\"&'<>") . "</protect>";
}

$tag = "security";
$file = "bts-$tag.txt";
open(BTS, "/srv/qa.debian.org/data/$file") || die "Cannot open $file";
$report_date = (stat BTS)[9];
$counter = <BTS>;
chomp($counter);
<BTS>; # ignore empty line
print "<P>\n";
if ($counter) {
    print "Number of packages tagged <I>$tag</I>: ", protect($counter);
} else {
    print "There are currently no bugs tagged <I>$tag</I>.";
}
print "\n\n";

$old_package = "";
while($package = <BTS>) {
    chomp($package);
    $bugid = <BTS>;
    chomp($bugid);
    $subject = <BTS>;
    chomp($subject);
    $severity = <BTS>;
    chomp($severity);
    $tags = <BTS>;
    chomp($tags);
    <BTS>; # ignore empty line

    if ($package ne $old_package) {
        print "<A NAME =\"", protect($package), "\"></A>",
              "<H2><A HREF = \"https://packages.debian.org/", protect($package),
              "\">", protect($package), "</A></H2>\n";
    }
    print "<PRE>\n";
    print "<A HREF = \"https://bugs.debian.org/", protect($bugid), "\">",
          "#", protect($bugid), "</A>: ", protect($subject), "\n";
    print "Severity: ", protect($severity), "\n";
    print "Tags: ", protect($tags), "\n";
    print "</PRE><P>\n";
    $old_package = $package;
}
close(BTS);

print "\n";
print "This listing was last updated on ";
print strftime '%a, %d %b %Y', gmtime $report_date;

:>

