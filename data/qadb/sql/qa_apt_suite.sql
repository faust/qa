BEGIN;

SET search_path TO apt;

-- architecture table

CREATE TEMP TABLE a ( architecture text ) ON COMMIT DROP;
INSERT INTO a VALUES ('source');
INSERT INTO a VALUES ('alpha');
INSERT INTO a VALUES ('amd64');
INSERT INTO a VALUES ('arm');
INSERT INTO a VALUES ('armel');
INSERT INTO a VALUES ('hppa');
INSERT INTO a VALUES ('hurd-i386');
INSERT INTO a VALUES ('i386');
INSERT INTO a VALUES ('ia64');
INSERT INTO a VALUES ('kfreebsd-amd64');
INSERT INTO a VALUES ('kfreebsd-i386');
INSERT INTO a VALUES ('mips');
INSERT INTO a VALUES ('mipsel');
INSERT INTO a VALUES ('powerpc');
INSERT INTO a VALUES ('s390');
INSERT INTO a VALUES ('sparc');
-- no 'binary' here

INSERT INTO architecture
	SELECT architecture FROM a
	WHERE architecture NOT IN (SELECT architecture FROM architecture);
INSERT INTO architecture
	SELECT 'all' WHERE 'all' NOT IN (SELECT architecture FROM architecture);
INSERT INTO architecture
	SELECT 'binary' WHERE 'binary' NOT IN (SELECT architecture FROM architecture);

-- suite table

CREATE TEMP TABLE s ( suite text ) ON COMMIT DROP;
INSERT INTO s VALUES ('lenny');
INSERT INTO s VALUES ('squeeze');
INSERT INTO s VALUES ('wheezy');
INSERT INTO s VALUES ('jessie');
INSERT INTO s VALUES ('sid');
INSERT INTO s VALUES ('experimental');

CREATE TEMP TABLE c ( component text ) ON COMMIT DROP;
INSERT INTO c VALUES ('main');
INSERT INTO c VALUES ('contrib');
INSERT INTO c VALUES ('non-free');

CREATE TEMP TABLE tmp_suite (
	archive text NOT NULL,
	suite text NOT NULL,
	component text NOT NULL,
	architecture text NOT NULL
) ON COMMIT DROP;

INSERT INTO tmp_suite (archive, suite, component, architecture)
	SELECT 'debian', suite, component, architecture FROM s, c, a;
INSERT INTO tmp_suite (archive, suite, component, architecture)
	SELECT 'debian', suite, component||'/debian-installer', architecture FROM s, c, a
	WHERE architecture <> 'source';
INSERT INTO tmp_suite (archive, suite, component, architecture)
	SELECT 'debian', suite||'-proposed-updates', component, architecture FROM s, c, a
	WHERE s.suite NOT IN ('sid', 'experimental');
INSERT INTO tmp_suite (archive, suite, component, architecture)
	SELECT 'debian', suite||'-updates', component, architecture FROM s, c, a
	WHERE s.suite IN ('squeeze');

INSERT INTO tmp_suite (archive, suite, component, architecture)
	SELECT 'debian-security', suite, 'updates/'||component, architecture FROM s, c, a
	WHERE s.suite NOT IN ('sid', 'experimental');

INSERT INTO tmp_suite (archive, suite, component, architecture)
	SELECT 'debian-backports', suite||'-backports', component, architecture FROM s, c, a
	WHERE s.suite IN ('lenny', 'squeeze');
INSERT INTO tmp_suite (archive, suite, component, architecture)
	SELECT 'debian-backports', suite||'-backports-sloppy', component, architecture FROM s, c, a
	WHERE s.suite IN ('lenny');

-- delete unsupported architectures
DELETE FROM tmp_suite WHERE architecture = 'arm' AND
	suite LIKE 'squeeze%' OR suite IN ('sid', 'experimental');
DELETE FROM tmp_suite WHERE architecture = 'armel' AND
	suite LIKE 'etch%';
DELETE FROM tmp_suite WHERE architecture LIKE 'hurd%' AND
	suite NOT IN ('sid', 'experimental');
DELETE FROM tmp_suite WHERE architecture LIKE 'kfreebsd%' AND
	suite NOT IN ('sid', 'experimental');

-- copy missing data from temp table over
INSERT INTO suite (archive, suite, component, architecture)
	SELECT archive, suite, component, architecture
	FROM tmp_suite
	WHERE (archive, suite, component, architecture) NOT IN
		(SELECT archive, suite, component, architecture FROM suite);

COMMIT;
