#!/usr/bin/perl -w
#  Copyright (C) 2007-2018 Christoph Berg <myon@debian.org>
#  Using ideas from ddpo.py:
#  Copyright (C) 2002 Igor Genibel
#  Copyright (C) 2005, 2006, 2007 Christoph Berg <myon@debian.org>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

use strict;
use DB_File;
use AptPkg::Config qw($_config);
use AptPkg::System qw($_system);
$_system = $_config->system;
my $aptvs = $_system->versioning;

my $db_filename = "archive-new.db";
my $ftp = "/srv/qa.debian.org/ftp/debian/dists";
my $security_ftp = "/srv/qa.debian.org/ftp/debian-security/dists";
my @components = qw/ main contrib non-free /;

my %db;
my $db_file = tie %db, "DB_File", $db_filename, O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE
    or die "Can't open database $db_filename : $!";

my %packages;
my %mixedcase_name;
my %mixedcase_pkg;

sub extract_suite
{
    # directory prefix, suite, suite abbreviation, component, extract-full-info
    my ($prefix, $suite, $s, $component, $full) = @_;
    my $fname = "$prefix/$suite/$component/source/Sources";
    my $decompressor = "cat";
    for my $compression (qw(xz bz2 gz)) {
        if (-f "$fname.$compression") {
            $fname = "$fname.$compression";
            $decompressor = "xzcat" if ($compression eq 'xz');
            $decompressor = "bzcat" if ($compression eq 'bz2');
            $decompressor = "zcat" if ($compression eq 'gz');
            last;
        }
    }
    die "$fname not found" unless (-f $fname);
    open F, "$decompressor $fname |" or die "$fname: $!";

    # parse packages file, keeping only information about the latest version
    my ($package, $maintainer, $architecture, $binary, $section, $priority,
        $version, $uploaders, $extrasourceonly);
    my %pkgdata;
    while (<F>) {
        $package = $1 if /^Package: (.+)/;
        $maintainer = $1 if /^Maintainer: (.+)/;
        $architecture = $1 if /^Architecture: (.+)/;
        $binary = $1 if /^Binary: (.+)/;
        $section = $1 if /^Section: (.+)/;
        $priority = $1 if /^Priority: (.+)/;
        $version = $1 if /^Version: (.+)/;
        $uploaders = $1 if /^Uploaders: (.+)/;
        $extrasourceonly = $1 if /^Extra-Source-Only: (.+)/; # this package stanza should be ignored

        if (/^$/) {
            if (exists $pkgdata{$package}) {
                if ($extrasourceonly or $aptvs->compare ($version, $pkgdata{$package}->{version}) <= 0) {
                    ($package, $maintainer, $architecture, $binary, $section, $priority,
                        $version, $uploaders, $extrasourceonly) = ();
                    next;
                }
            }

            $pkgdata{$package} = {
                maintainer => $maintainer,
                architecture => $architecture,
                binary => $binary,
                section => $section,
                priority => $priority,
                version => $version,
                uploaders => $uploaders,
            };

            ($package, $maintainer, $architecture, $binary, $section, $priority,
                $version, $uploaders, $extrasourceonly) = ();
        }
    }
    close F;

    # now push package information into the database
    foreach my $package (keys %pkgdata) {
        $db{"$s:$package"} = $pkgdata{$package}->{version};
        $db{"arch:$package"} //= $pkgdata{$package}->{architecture};
        $db{"bin:$package"} //= join " ", split(/, */, $pkgdata{$package}->{binary});
        $db{"sec:$package"} //= $pkgdata{$package}->{section};
        $db{"pri:$package"} //= $pkgdata{$package}->{priority};
        $pkgdata{$package}->{maintainer} =~ /(.+) <(.+)>/ or warn "$fname:$.: syntax error in $pkgdata{$package}->{maintainer}";
        my ($name, $mail) = ($1, $2);

        if ($full or not exists $db{"maint:$package"}) {
            # Add package to maintainer if "full" or if we haven't seen the package before
            # (i.e. include removed packages, but don't include packages
            # that have a new maintainer in a newer distribution)
            $db{"name:$mail"} //= $name;
            $mixedcase_name{lc($mail)}{$mail} = 1;
            $packages{$mail}->{$component}->{$package} = 1;

            if ($pkgdata{$package}->{uploaders}) {
                # Unfortunately Email::Address wants dots in names quoted
                # in a way we currently don't enforce
                #my @uploaders = Email::Address->parse($pkgdata{$package}->{uploaders});
                my @uploaders = split(/>\K\s*,\s*/, $pkgdata{$package}->{uploaders});
                $db{"com:$package"} = scalar @uploaders;
                foreach my $uploader (@uploaders) {
                    my ($name, $mail);
                    if ($uploader =~ /^\S+$/) {
                        ($name, $mail) = ("(unknown)", $uploader);
                        warn "Uploader without name: $package $uploader";
                    } else {
                        $uploader =~ /(.+) <(.+)>/ or warn "$fname:$.: syntax error in $uploader";
                        ($name, $mail) = ($1, $2);
                        $db{"name:$mail"} //= $name;
                        $mixedcase_name{lc($mail)}{$mail} = 1;
                    }
                    $packages{$mail}->{$component}->{$package} = 1;
                }
            }

            $db{"src:$_"} = $package foreach (split(/, */, $pkgdata{$package}->{binary}));
        }

        $db{"maint:$package"} //= $mail; # we have seen this package now
    }
}

sub extract
{
    my ($prefix, $suite, $s, $full) = @_;

    my $symlink = $suite;
    $symlink =~ s%/%-%g;

    my $file;
    $file = readlink( "$ftp/$symlink" ) if( -l "$ftp/$symlink" );
    $file = undef if( defined $file and $file !~ /^[a-z\-]+$/ );

    $db{"readlink:$symlink"} = $file if( defined $file );

    foreach my $component (@components) {
        extract_suite($prefix, $suite, $s, $component, $full);
    }
}

# read suite -> codename mapping
my ($oldoldstable, $oldstable);
open R, "/srv/qa.debian.org/data/RELEASES" or die "/srv/qa.debian.org/data/RELEASES: $!";
while (<R>) {
    next unless /(\S+)\s+(\S+)/; # suite codename [tags]
    $oldoldstable = $2 if ($1 eq 'oldoldstable');
    $oldstable = $2 if ($1 eq 'oldstable');
}
close R;

# extract info from newest to oldest dist
extract ($ftp, "experimental", "e", 1);
extract ($ftp, "unstable", "u", 1);

extract ($ftp, "testing", "t");
extract ($ftp, "testing-proposed-updates", "tpu");
extract ($security_ftp, "testing/updates", "t-sec");

extract ($ftp, "stable", "s");
extract ($ftp, "stable-updates", "su");
extract ($ftp, "proposed-updates", "spu");
extract ($security_ftp, "stable/updates", "s-sec");

extract ($ftp, "oldstable", "olds") if -d "$ftp/oldstable";
extract ($security_ftp, "oldstable/updates", "olds-sec") if -d "$security_ftp/oldstable/updates";
extract ($ftp, "oldstable-proposed-updates", "ospu") if -d "$ftp/oldstable-proposed-updates";
# $dist-lts doesn't have a symlink
extract ($ftp, "$oldstable-lts", "oslts") if -d "$ftp/$oldstable-lts";

extract ($ftp, "oldoldstable", "oldolds") if -d "$ftp/oldoldstable";
extract ($security_ftp, "oldoldstable/updates", "oldolds-sec") if -d "$security_ftp/oldoldstable/updates";
extract ($ftp, "oldoldstable-proposed-updates", "oospu") if -d "$ftp/oldoldstable-proposed-updates";
# $dist-lts doesn't have a symlink
extract ($ftp, "$oldoldstable-lts", "ooslts") if -d "$ftp/$oldoldstable-lts";

# write package lists
foreach my $maintainer (sort keys %packages) {
    foreach my $component (keys %{$packages{$maintainer}}) {
        $db{"$maintainer:$component"} = join " ",
            map { s/$/#/ if $db{"maint:$_"} ne $maintainer; $_; } # mark co-maintainership
            sort keys %{$packages{$maintainer}->{$component}};
        $mixedcase_pkg{lc($maintainer)}{$maintainer} = 1;
    }
    print $db{"name:$maintainer"} . " <$maintainer>\n";
}

# vim:sw=4:softtabstop=4:expandtab
