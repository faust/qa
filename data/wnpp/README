wnpp-lint
---------

wnpp-lint is a script which checks all WNPP bugs (for further information
about the WNPP, check out https://www.debian.org/devel/wnpp) if they have
been properly done.  This means that it checks the Subject line, the
severity and it also checks for orphaned packages which are not in Debian.

wnpp-lint runs interactively and spawns mutt with appropriate
control@bugs.debian.org or nnnn-done@bugs.debian.org messages.


wnpp-publicize
--------------

wnpp-publicize shows a listing of orphaned packages for which no WNPP bugs
have been filed yet.  For each package printed out by wnpp-publicize an
O: bug should be filed against the "wnpp" pseudo package.  Since
wnpp-publicize uses projectB (katie's SQL database) to get a listing of all
source packages, the script has to be run on ftp-master.debian.org or a
mirror thereof (currently merkel.debian.org).

If the script cannot parse the bug's subject line (into tag and source) the
script will think there is no bug report although there is.  I will try to
make the code to parse the subject line smarter and more robust but until
that happens, please make sure to check that no bugs have in fact been filed
before posting one.

When you have verified that no bug has been filed already, you can use
wnpp-orphan --action qa from /srv/qa.debian.org/mia to generate a template
to file a WNPP bug which contains the relevant information already.


wnpp-disposable
---------------

wnpp-disposable shows a listing of orphaned packages which are not in
stable.  These packages can easily be removed since they have not been
part of an official release.  This script should be run on
ftp-master.debian.org or its mirror when a new release is in preparation.

