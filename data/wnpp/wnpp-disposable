#!/usr/bin/env python

# Show a listing of orphaned packages which are not in stable.
# These packages can easily be removed since they have not been
# part of an official release.
# Copyright (C) 2002, 2004, 2005  Martin Michlmayr <tbm@cyrius.com>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import string, sys
import wnpp
sys.path.append("/srv/ftp.debian.org/dak")
import apt_pkg, pg, utils, db_access


def help():
    print """Usage: wnpp-disposable [OPTIONS]...
Show a listing of orphaned packages which are not in stable.

Options:
  -d, --days=DAYS           Show packages orphaned more than DAYS ago
  -v, --verbose=VALUE       Verbose; enable verbose output
                            Multiple -v increase verbosity
  -t, --tag=TAG             Only show WNPP bugs with this tag (default: %s)

TAG can be a space separated list, e.g.
  --tag=\"O ITA\"
""" % Options["Tag"]
    sys.exit(1)

def parse_args():
    global Options

    apt_pkg.init()
    Cnf = apt_pkg.newConfiguration()
    apt_pkg.read_config_file_isc(Cnf, wnpp.config)

    Arguments = [("h", "help", "Disposable::Options::Help"),
                 ("d", "days", "Disposable::Options::TTL", "IntLevel"),
                 ("v", "verbose", "Disposable::Options::Verbose", "IntLevel"),
                 ("s", "sort", "Disposable::Options::Sort", "HasArg"),
                 ("t", "tag", "Disposable::Options::Tag", "HasArg")]

    apt_pkg.parse_commandline(Cnf, Arguments, sys.argv)
    Options = Cnf.SubTree("Disposable::Options")

    if Options["Help"]:
        help()

    tags = []
    for tag in string.split(string.upper(Options["Tag"])):
        if tag in wnpp.exists:
            tags.append(tag)
        else:
            utils.warn("Tag '%s' not recognised." % tag)
    if not tags:
        utils.fubar("No correct tag given.")

    sort = Cnf.ValueList("Disposable::Options::Sorts")[0]
    if Options.has_key("Sort"):
        if Options["Sort"] in Cnf.ValueList("Disposable::Options::Sorts"):
            sort = Options["Sort"]
        else:
            print "W: Unknown sort option '%s', using default '%s'." % (Options["Sort"], sort)
    sort_function = globals()["sort_by_%s" % sort]

    return (tags, sort_function)


# sort by age: oldest first
def sort_by_age(a, b):
    return cmp(info[b]["age"], info[a]["age"])

def sort_by_name(a, b):
    return cmp(a, b)

def main(tags, sort_function):
    # Connect to the BTS LDAP interface to get a listing of all orphaned
    # packages.  Only consider those which are older than a specific
    # age.
    ldap_result = wnpp.query(["debbugsID", "debbugsTitle", "debbugsState", "debbugsSubmitter", "debbugsDate"])
    if not ldap_result:
        print "Problem with LDAP search"
        sys.exit(1)
    else:
        global info
        orphaned = []
        info = {}
        for _, dict in ldap_result:
            if not dict:
                continue
            if dict["debbugsState"][0] == "done":
                continue
            # Give people some time to adopt orphaned packages
            age = wnpp.date2days(dict["debbugsDate"][0])
            if age < int(Options["TTL"]):
                continue
            result = wnpp.proper_subject.search(dict["debbugsTitle"][0])
            if result:
                tag = result.group(1)
                package = string.lower(result.group(2))
                description = result.group(3)
            else:
                continue
            if tag not in tags:
                continue

            orphaned.append(package)
            info[package] = {}
            info[package]["bugid"] = int(dict["debbugsID"][0])
            info[package]["tag"] = tag
            info[package]["description"] = description
            info[package]["originater"] = dict["debbugsSubmitter"][0]
            info[package]["age"] = age

    if not orphaned:
        # Nothing to do, exit
        sys.exit(0)

    Cnf = None
    projectB = pg.connect("projectb")
    db_access.init(Cnf, projectB)

    # First, take the listing of packages orphaned in WNPP and get a
    # listing which of these packages are actually available in unstable.
    # This is necessary since a) a WNPP bug can be filed although the
    # package does not exist (due to a typo or because the package has
    # been removed) or b) the package resides in another archive (main vs
    # non-US).
    q = projectB.query("SELECT s.source FROM source s, src_associations sa WHERE sa.suite = %d AND sa.source = s.id AND s.source IN (%s)" % (db_access.get_suite_id("unstable"), string.join(map(repr, orphaned), ", ")))
    unstable = map(lambda x: x[0], q.getresult())
    unstable.sort(sort_function)
    if not unstable:
        # Nothing to do, exit
        sys.exit(0)
    # Get a listing of packages in stable based on those just acquired
    # for unstable.
    q = projectB.query("SELECT s.source FROM source s, src_associations sa WHERE sa.suite = %d AND sa.source = s.id AND s.source IN (%s)" % (db_access.get_suite_id("stable"), string.join(map(repr, unstable), ", ")))
    stable = map(lambda x: x[0], q.getresult())
    # Check for packages in unstable that are not in stable and print them.
    for package in unstable:
        if package not in stable:
            if not int(Options["Verbose"]):
                print package
            else:
                print "#%6d: %s: %s -- %s" % (info[package]["bugid"], info[package]["tag"], package, info[package]["description"])
                if int(Options["Verbose"]) > 1:
                    print "Reported by %s; %d days ago." % (info[package]["originater"], info[package]["age"])
                    print


(tags, sort_function) = parse_args()
main(tags, sort_function)

# vim: ts=4:expandtab:shiftwidth=4:
